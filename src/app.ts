import securityFactory from "./factories/security-factory";
import surveyFactory from "./factories/survey-factory";
import featureFactory from "./factories/feature-factory";
import writeFile from "./utilities/write-file";

const notification = surveyFactory({
  hostAppKey: "jira",
  appKey: "com.onresolve.jira.groovy.groovyrunner",
  appVersion: "1.0.5",
  words: "",
  survey: {
    description:
      "Help us to help you: if you use our documentation, tell us how we can improve.",
    surveyUrl:
      "https://adaptavistuk.fra1.qualtrics.com/jfe/form/SV_4I9Is8vesvbBLfg?utm_source=sr4cop&utm_medium=banner",
    surveyText: "Share your thoughts",
    reminderText: "Maybe later",
    title: "Use our documentation?",
    publishDate: "November 17 2021",
    endDate: "December 16 2021",
  },
});

const security = securityFactory({
  appKey: "com.onresolve.jira.groovy.groovyrunner",
  hostAppKey: "bitbucket",
  appVersion: "All",
  words: "",
  actions: [],
});

const feature = featureFactory({
  appKey: "com.onresolve.jira.groovy.groovyrunner",
  hostAppKey: "bitbucket",
  appVersion: "2.1.2",
  words: "",
  actions: [{ text: "blah", url: "https://www.foobar.baz" }],
});

try {
  writeFile(security);
} catch (e) {
  console.log(e);
}
