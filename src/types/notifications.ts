type NotificationType = "survey" | "security" | "feature";

export type HostAppKey = "jira" | "confluence" | "bamboo" | "bitbucket";

export type AppKey =
  | "com.onresolve.jira.groovy.groovyrunner"
  | "com.onresolve.confluence.groovy.groovyrunner"
  | "com.onresolve.bamboo.groovy.groovyrunner"
  | "com.onresolve.stash.groovy.groovyrunner";

export type SurveyProperties = {
  publishDate: string;
  endDate: string;
  description: string;
  surveyText: string;
  surveyUrl: string;
  reminderText: string;
  title: string;
};

export type Action = {
  url: string;
  text: string;
};

interface InAppNotification {
  uuid: string;
  notificationType: NotificationType;
  hostAppKey: HostAppKey;
  appKey: AppKey;
  appVersion: string; //  format is either "All" or "1.2.3"
  words: string;
}

export interface Survey extends InAppNotification {
  survey: SurveyProperties;
  notificationType: "survey";
}

export interface Security extends InAppNotification {
  actions: Action[];
  notificationType: "security";
}

export interface Feature extends InAppNotification {
  actions: Action[];
  notificationType: "feature";
}
