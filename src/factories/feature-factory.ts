import { HostAppKey, AppKey, Feature, Action } from "../types/notifications";
import { v4 as uuid } from "uuid";

interface Payload {
  hostAppKey: HostAppKey;
  appKey: AppKey;
  appVersion: string;
  words: string;
  actions: Action[];
}

const featureFactory = (payload: Payload): Feature => {
  const { hostAppKey, appKey, appVersion, words, actions } = payload;

  return {
    uuid: uuid(),
    notificationType: "feature",
    hostAppKey,
    appKey,
    appVersion,
    words,
    actions,
  };
};

export default featureFactory;
