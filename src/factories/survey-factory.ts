import {
  Survey,
  HostAppKey,
  AppKey,
  SurveyProperties,
} from "../types/notifications";
import { v4 as uuid } from "uuid";

interface Payload {
  hostAppKey: HostAppKey;
  appKey: AppKey;
  appVersion: string;
  words: string;
  survey: SurveyProperties;
}

const surveyFactory = (payload: Payload): Survey => {
  const { hostAppKey, appKey, appVersion, words } = payload;
  const {
    publishDate,
    endDate,
    description,
    surveyText,
    surveyUrl,
    reminderText,
    title,
  } = payload.survey;

  return {
    uuid: uuid(),
    notificationType: "survey",
    hostAppKey,
    appKey,
    appVersion,
    words,
    survey: {
      description,
      publishDate,
      endDate,
      reminderText,
      surveyText,
      surveyUrl,
      title,
    },
  };
};

export default surveyFactory;
