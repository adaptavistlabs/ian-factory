import { HostAppKey, AppKey, Security, Action } from "../types/notifications";
import { v4 as uuid } from "uuid";

interface Payload {
  hostAppKey: HostAppKey;
  appKey: AppKey;
  appVersion: string;
  words: string;
  actions: Action[];
}

const securityFactory = (payload: Payload): Security => {
  const { hostAppKey, appKey, appVersion, words, actions } = payload;

  return {
    uuid: uuid(),
    notificationType: "security",
    hostAppKey,
    appKey,
    appVersion,
    words,
    actions,
  };
};

export default securityFactory;
