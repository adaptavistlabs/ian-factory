import * as fs from "fs";

const makeDir = () => {
  if (fs.existsSync("out")) return;
  fs.mkdir("out", (err) => {
    if (err) throw err;
  });
};

export default makeDir;
