import * as fs from "fs";
import { Feature, Security, Survey } from "../types/notifications";
import makeDir from "./make-dir";

function writeFile(notification: Survey | Security | Feature) {
  makeDir();
  fs.writeFileSync(
    `./out/${+new Date()}-${notification.notificationType}.json`,
    JSON.stringify(notification, null, 2)
  );
}

export default writeFile;
