# In App Notifications Factory

This project is supposed to provide a consistent way for creating notifications and automating some processes for In App Notifications project.

There are 3 types of notifications: survey, security and feature.

## Setup

1. clone repo
2. run `npm i` in project root

## Creating notifications

1. Open the `app.ts` file.
2. Create your notification by using one of the available factories (survey, security, feature)
3. Pass the notification into `writeFile` function
4. Run `npm start`, which generates json to `out` directory.
5. Copy the contents of generated JSON
6. Add it to DynamoDB table

### Example 1 - Survey:

```js
import surveyFactory from "./factories/survey-factory";
import writeFile from "./utilities/write-file";

const notification = surveyFactory({
  hostAppKey: "jira",
  appKey: "com.onresolve.jira.groovy.groovyrunner",
  appVersion: "All",
  words: "",
  survey: {
    description: "Help us to help you, tell us how we can improve.",
    surveyUrl: "https://foobar.com/form/123",
    surveyText: "Share your thoughts",
    reminderText: "Maybe later",
    title: "Use our documentation?",
    publishDate: "November 17 2021",
    endDate: "December 16 2021",
  },
});

try {
  writeFile(notification);
} catch (e) {
  console.log(e);
}
```

### Example 2 - Security:

```js
import securityFactory from "./factories/security-factory";
import writeFile from "./utilities/write-file";

try {
  writeFile(
    securityFactory({
      hostAppKey: "confluence",
      appKey: "com.onresolve.confluence.groovy.groovyrunner",
      appVersion: "1.2.3",
      words: "blah",
      actions: [{ text: "blah", url: "https://www.foo.bar" }],
    })
  );
} catch (e) {
  console.log(e);
}
```

### Example 3 - Feature:

```js
import featureFactory from "./factories/feature-factory";
import writeFile from "./utilities/write-file";

const feature = featureFactory({
  appKey: "com.onresolve.jira.groovy.groovyrunner",
  hostAppKey: "bitbucket",
  appVersion: "2.1.2",
  words: "",
  actions: [{ text: "blah", url: "https://www.foobar.baz" }],
});

try {
  writeFile(feature);
} catch (e) {
  console.log(e);
}
```